import 'package:dio/dio.dart';

import '../../models/weather_responce.dart';

class RemoteDataSource {
  static const BASE_URL = "https://api.open-meteo.com/";
  static const String apiEndpoint = BASE_URL + "v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m";
  final Dio _dio = Dio();

  Future<WeatherResponse> loadData() async{
    final Response response = await _dio.get(
      apiEndpoint,
    );
    return WeatherResponse.fromJson(response.data);
  }

}
