import 'package:weather_app/data/datasources/remote/remote_datasource.dart';

import '../../data/models/weather_responce.dart';

class WeatherRepository {
  RemoteDataSource remoteDataSource = RemoteDataSource();

  Future<WeatherResponse> getData() {
    return remoteDataSource.loadData();
  }
}
