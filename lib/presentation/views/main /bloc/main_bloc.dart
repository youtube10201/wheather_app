import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:weather_app/data/models/weather_responce.dart';
import 'package:weather_app/domain/repositories/weather_pepository.dart';

part 'main_event.dart';

part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  WeatherRepository _weatherRepository = WeatherRepository();

  MainBloc() : super(MainInitial()) {
    on<MainEventLoad>((event, emit) async {
      emit(MainStateLoading());
      try {
        WeatherResponse response = await _weatherRepository.getData();
        emit(MainStateLoaded(response));
      } on Exception catch (_, e) {
        if (kDebugMode) {
          print(e);
        }
      }
    });
  }
}
