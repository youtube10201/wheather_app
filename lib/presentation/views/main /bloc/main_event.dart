part of 'main_bloc.dart';

@immutable
abstract class MainEvent {}

class MainEventLoad extends MainEvent {}
