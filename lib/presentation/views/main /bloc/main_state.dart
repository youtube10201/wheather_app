part of 'main_bloc.dart';

@immutable
abstract class MainState {}

class MainInitial extends MainState {}

class MainStateLoading extends MainState {}

class MainStateLoaded extends MainState {
  WeatherResponse response;

  MainStateLoaded(this.response);
}
