import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/main_bloc.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late MainBloc _mainBloc;

  @override
  void initState() {
    _mainBloc = MainBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _mainBloc,
      child: Scaffold(
        body: BlocBuilder<MainBloc, MainState>(
          builder: (context, state) {
            if (state is MainStateLoading) {
              const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is MainStateLoaded) {
              return SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                        '${state.response.latitude.toString()} ${state.response.longitude.toString()} ${state.response.timezone.toString()}'),
                    ElevatedButton(
                      onPressed: () {
                        _mainBloc.add(MainEventLoad());
                      },
                      child: const Text('Load Data'),
                    ),
                  ],
                ),
              );
            }
            return Center(
              child: ElevatedButton(
                onPressed: () {
                  _mainBloc.add(MainEventLoad());
                },
                child: const Text('Load Data'),
              ),
            );
          },
        ),
      ),
    );
  }
}
